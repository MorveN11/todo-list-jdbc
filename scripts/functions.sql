USE todo_list_db;

DELIMITER $$
CREATE FUNCTION get_todo_count_by_state(state_to_search ENUM ('COMPLETED', 'IN PROGRESS', 'DONE',
    'TODO'))
    RETURNS INT
    DETERMINISTIC
BEGIN
    DECLARE count INT;
    SELECT COUNT(*) INTO count FROM state WHERE name = state_to_search;
    RETURN count;
END
$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE insert_todo(IN todo_title VARCHAR(255),
                             IN todo_description VARCHAR(255),
                             IN todo_state ENUM ('COMPLETED', 'IN PROGRESS', 'DONE', 'TODO'),
                             IN todo_priority ENUM ('HIGH', 'MEDIUM', 'LOW'))
BEGIN
    DECLARE last_id INT DEFAULT 0;

    INSERT INTO todo (title, description)
    VALUES (todo_title, todo_description);

    SELECT LAST_INSERT_ID() INTO last_id;

    INSERT INTO state (todo_id, name)
    VALUES (last_id, todo_state);

    INSERT INTO priority (todo_id, name)
    VALUES (last_id, todo_priority);
END
$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION PASSWORD2(pass_in varchar(255)) RETURNS varchar(255)
    DETERMINISTIC
BEGIN
    DECLARE n_pass VARCHAR(255);
    SET n_pass = CONCAT('*', UPPER(SHA1(UNHEX(SHA1(pass_in)))));
    RETURN n_pass;
END
$$
DELIMITER ;
