USE todo_list_db;

DELIMITER $$
CREATE TRIGGER todo_updated_at_trigger
    BEFORE UPDATE
    ON todo
    FOR EACH ROW
BEGIN
    SET NEW.updated_at = NOW();
END
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER priority_updated_at_trigger
    BEFORE UPDATE
    ON priority
    FOR EACH ROW
BEGIN
    SET NEW.updated_at = NOW();
END
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER state_updated_at_trigger
    BEFORE UPDATE
    ON state
    FOR EACH ROW
BEGIN
    SET NEW.updated_at = NOW();
END
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER tag_updated_at_trigger
    BEFORE UPDATE
    ON tag
    FOR EACH ROW
BEGIN
    SET NEW.updated_at = NOW();
END
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER user_updated_at_trigger
    BEFORE UPDATE
    ON user
    FOR EACH ROW
BEGIN
    SET NEW.updated_at = NOW();
END
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER todo_tag_updated_at_trigger
    BEFORE UPDATE
    ON todo_tag
    FOR EACH ROW
BEGIN
    SET NEW.updated_at = NOW();
END
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER todo_user_updated_at_trigger
    BEFORE UPDATE
    ON todo_user
    FOR EACH ROW
BEGIN
    SET NEW.updated_at = NOW();
END
$$
DELIMITER ;
