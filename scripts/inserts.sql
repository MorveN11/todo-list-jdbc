USE todo_list_db;

CALL insert_todo('Do laundry', 'Wash and fold clothes', 'TODO', 'HIGH');
CALL insert_todo('Buy groceries', 'Milk, bread, eggs', 'COMPLETED', 'LOW');
CALL insert_todo('Pay bills', 'Electricity, water, internet', 'TODO', 'MEDIUM');
CALL insert_todo('Call mom', 'Check on her health', 'COMPLETED', 'HIGH');
CALL insert_todo('Finish report', 'Submit by end of week', 'DONE', 'HIGH');
CALL insert_todo('Go for a run', 'Exercise for 30 minutes', 'TODO', 'LOW');
CALL insert_todo('Schedule dentist appointment', 'Check for available slots', 'IN PROGRESS',
                 'MEDIUM');
CALL insert_todo('Read book', 'Chapter 5 to 10', 'COMPLETED', 'LOW');
CALL insert_todo('Prepare presentation', 'Research and create slides', 'TODO', 'HIGH');
CALL insert_todo('Clean the house', 'Dust and vacuum all rooms', 'IN PROGRESS', 'MEDIUM');
CALL insert_todo('Fix leaking faucet', 'Call plumber for repairs', 'DONE', 'HIGH');
CALL insert_todo('Attend team meeting', 'Discuss project progress', 'TODO', 'LOW');
CALL insert_todo('Send birthday gift', 'Order online and ship to address', 'COMPLETED', 'HIGH');
CALL insert_todo('Update resume', 'Add recent work experience', 'TODO', 'MEDIUM');
CALL insert_todo('Book flight tickets', 'Check for best deals', 'IN PROGRESS', 'LOW');
CALL insert_todo('Renew gym membership', 'Pay annual fee', 'DONE', 'HIGH');
CALL insert_todo('Call insurance company', 'Discuss coverage options', 'TODO', 'MEDIUM');
CALL insert_todo('Plan weekend getaway', 'Research destinations and accommodations', 'COMPLETED',
                 'LOW');
CALL insert_todo('Organize paperwork', 'Sort and file important documents', 'TODO', 'HIGH');
CALL insert_todo('Start a new hobby', 'Painting or playing an instrument', 'IN PROGRESS', 'MEDIUM');

INSERT INTO tag(name)
VALUES ('HOME'),
       ('WORK'),
       ('PERSONAL'),
       ('HEALTH'),
       ('FINANCE'),
       ('TRAVEL');

INSERT INTO user (name, last_name, email, password, phone_number)
VALUES ('John', 'Doe', 'john.doe@example.com', PASSWORD2('password123'), '123-456-7890'),
       ('Jane', 'Smith', 'jane.smith@example.com', PASSWORD2('password456'), '987-654-3210'),
       ('Michael', 'Johnson', 'michael.johnson@example.com', PASSWORD2('password789'),
        '555-555-5555'),
       ('Emily', 'Davis', 'emily.davis@example.com', PASSWORD2('passwordabc'), '111-222-3333'),
       ('David', 'Wilson', 'david.wilson@example.com', PASSWORD2('passwordxyz'), '999-888-7777'),
       ('Sarah', 'Taylor', 'sarah.taylor@example.com', PASSWORD2('password123'), '444-444-4444'),
       ('Robert', 'Anderson', 'robert.anderson@example.com', PASSWORD2('password456'),
        '777-777-7777'),
       ('Jessica', 'Thomas', 'jessica.thomas@example.com', PASSWORD2('password789'),
        '222-333-4444'),
       ('William', 'Jackson', 'william.jackson@example.com', PASSWORD2('passwordabc'),
        '888-888-8888'),
       ('Melissa', 'Harris', 'melissa.harris@example.com', PASSWORD2('passwordxyz'),
        '666-666-6666'),
       ('Christopher', 'Lee', 'christopher.lee@example.com', PASSWORD2('password123'),
        '333-444-5555'),
       ('Amanda', 'Martin', 'amanda.martin@example.com', PASSWORD2('password456'), '555-666-7777'),
       ('Daniel', 'Thompson', 'daniel.thompson@example.com', PASSWORD2('password789'),
        '777-888-9999'),
       ('Stephanie', 'White', 'stephanie.white@example.com', PASSWORD2('passwordabc'),
        '111-222-3333'),
       ('Matthew', 'Hall', 'matthew.hall@example.com', PASSWORD2('passwordxyz'), '777-777-7777'),
       ('Laura', 'Taylor', 'laura.taylor@example.com', PASSWORD2('password123'), '444-555-6666'),
       ('Andrew', 'Smith', 'andrew.smith@example.com', PASSWORD2('password456'), '999-999-9999'),
       ('Jennifer', 'Brown', 'jennifer.brown@example.com', PASSWORD2('password789'),
        '222-333-4444'),
       ('Kevin', 'Clark', 'kevin.clark@example.com', PASSWORD2('passwordabc'), '888-999-0000'),
       ('Michelle', 'Johnson', 'michelle.johnson@example.com', PASSWORD2('passwordxyz'),
        '666-777-8888');

INSERT INTO todo_tag(todo_id, tag_id)
VALUES (1, 3),
       (1, 4),
       (2, 1),
       (2, 5),
       (3, 2),
       (3, 4),
       (4, 3),
       (4, 4),
       (5, 1),
       (5, 5),
       (6, 2),
       (6, 4),
       (7, 3),
       (7, 5),
       (8, 1),
       (8, 4),
       (9, 2),
       (9, 5),
       (10, 3),
       (10, 4),
       (11, 1),
       (11, 5),
       (12, 2),
       (12, 4),
       (13, 3),
       (13, 5),
       (14, 1),
       (14, 4),
       (15, 2),
       (15, 5),
       (16, 3),
       (16, 4),
       (17, 1),
       (17, 5),
       (18, 2),
       (18, 4),
       (19, 3),
       (19, 5),
       (20, 1),
       (20, 4);

INSERT INTO todo_user(todo_id, user_id)
VALUES (1, 20),
       (1, 19),
       (2, 18),
       (2, 17),
       (3, 16),
       (3, 15),
       (4, 14),
       (4, 13),
       (5, 12),
       (5, 11),
       (6, 10),
       (6, 9),
       (7, 8),
       (7, 7),
       (8, 6),
       (8, 5),
       (9, 4),
       (9, 3),
       (10, 2),
       (10, 1),
       (11, 20),
       (11, 19),
       (12, 18),
       (12, 17),
       (13, 16),
       (13, 15),
       (14, 14),
       (14, 13),
       (15, 12),
       (15, 11),
       (16, 10),
       (16, 9),
       (17, 8),
       (17, 7),
       (18, 6),
       (18, 5),
       (19, 4),
       (19, 3),
       (20, 2),
       (20, 1);
