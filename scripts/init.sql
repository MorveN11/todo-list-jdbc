DROP DATABASE IF EXISTS todo_list_db;

CREATE DATABASE IF NOT EXISTS todo_list_db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE todo_list_db;

SET GLOBAL log_bin_trust_function_creators = 1;

CREATE TABLE IF NOT EXISTS todo
(
    id          INT(11)      NOT NULL AUTO_INCREMENT,
    title       VARCHAR(100) NOT NULL,
    description VARCHAR(255) NOT NULL,
    created_at  DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at  DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS priority
(
    id         INT                            NOT NULL AUTO_INCREMENT,
    todo_id    INT UNIQUE                     NOT NULL,
    name       ENUM ('HIGH', 'MEDIUM', 'LOW') NOT NULL DEFAULT 'LOW',
    created_at DATETIME                       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME                       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (todo_id) REFERENCES todo (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS state
(
    id         INT                                               NOT NULL AUTO_INCREMENT,
    todo_id    INT UNIQUE                                        NOT NULL,
    name       ENUM ('COMPLETED', 'IN PROGRESS', 'DONE', 'TODO') NOT NULL DEFAULT 'TODO',
    created_at DATETIME                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (todo_id) REFERENCES todo (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS tag
(
    id         INT                 NOT NULL AUTO_INCREMENT,
    name       VARCHAR(100) UNIQUE NOT NULL,
    created_at DATETIME            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS user
(
    id            INT          NOT NULL AUTO_INCREMENT,
    name          VARCHAR(50)  NOT NULL,
    email         VARCHAR(255) NOT NULL UNIQUE,
    password      VARCHAR(255) NOT NULL,
    last_name     VARCHAR(50),
    phone_number  VARCHAR(50),
    profile_image LONGBLOB,
    created_at    DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at    DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS todo_tag
(
    todo_id    INT      NOT NULL,
    tag_id     INT      NOT NULL,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (todo_id, tag_id),
    FOREIGN KEY (todo_id) REFERENCES todo (id) ON DELETE CASCADE,
    FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS todo_user
(
    todo_id     INT      NOT NULL,
    user_id     INT      NOT NULL,
    assigned_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (todo_id, user_id),
    FOREIGN KEY (todo_id) REFERENCES todo (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
);
