package todo.list.jdbc.todo;

import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import todo.list.jdbc.todo.entities.TodoEntity;

class TodoEntityTest {

  private TodoEntity todoEntity;

  @BeforeEach
  void setUp() {
    this.todoEntity = new TodoEntity(1,
                                     "title",
                                     "description",
                                     LocalDate.of(2021,
                                                  1,
                                                  1),
                                     LocalDate.of(2021,
                                                  1,
                                                  2));
  }

  @Test
  void testGetId() {
    assert this.todoEntity.getId() == 1;
  }

  @Test
  void testGetTitle() {
    assert this.todoEntity.getTitle().equals("title");
  }

  @Test
  void testGetDescription() {
    assert this.todoEntity.getDescription().equals("description");
  }

  @Test
  void testGetCreatedAt() {
    assert this.todoEntity.getCreatedAt().equals(LocalDate.of(2021,
                                                              1,
                                                              1));
  }

  @Test
  void testGetUpdatedAt() {
    assert this.todoEntity.getUpdatedAt().equals(LocalDate.of(2021,
                                                              1,
                                                              2));
  }

  @Test
  void testToString() {
    String todo = """ 
            Todo
            id: 1
            title: title
            description: description
            createdAt: %s
            updatedAt: %s
            """.formatted(LocalDate.of(2021,
                                       1,
                                       1),
                          LocalDate.of(2021,
                                       1,
                                       2));
    assert this.todoEntity.toString().equals(todo);
  }
}
