package todo.list.jdbc.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import todo.list.jdbc.interfaces.Service;
import todo.list.jdbc.user.entities.UserEntity;

/**
 * This class models the user.
 */
public class UserService extends Service<UserEntity> {

  public UserService(Connection connection) {
    super(connection);
  }

  @Override
  public void create(Object object) {
  }

  @Override
  public UserEntity get(int id) {
    return null;
  }

  @Override
  public void update(Object object, int id) {
  }

  @Override
  public void delete(int id) {
  }

  @Override
  public List<UserEntity> listAll() {
    return null;
  }

  /**
   * This method is used to log in the user.
   *
   * @param email    The email of the user.
   * @param password The password of the user.
   * @return The user entity.
   */
  public UserEntity login(String email, String password) {
    String loginUserQuery = """
            SELECT * FROM user WHERE email = ? AND password = PASSWORD2(?);
            """;
    try (PreparedStatement preparedStatement = this.connection.prepareStatement(loginUserQuery)) {
      preparedStatement.setString(1,
                                  email);
      preparedStatement.setString(2,
                                  password);
      var rs = preparedStatement.executeQuery();
      if (rs.next()) {
        return new UserEntity(rs);
      }
    } catch (Exception e) {
      throw new RuntimeException("Error getting user: " + e.getMessage(),
                                 e);
    }
    return null;
  }
}
