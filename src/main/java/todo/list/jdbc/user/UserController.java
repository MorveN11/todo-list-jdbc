package todo.list.jdbc.user;

import java.sql.Connection;
import todo.list.jdbc.user.entities.UserEntity;

/**
 * This class models the user.
 */
public class UserController {

  protected final UserService userService;

  public UserController(Connection connection) {
    this.userService = new UserService(connection);
  }

  public UserEntity getUser(int id) {
    return this.userService.get(id);
  }

  public UserEntity login(String email, String password) {
    return this.userService.login(email,
                                  password);
  }
}
