package todo.list.jdbc.user.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import todo.list.utils.CommonComponents;

/**
 * This class models the user.
 */
public class UserEntity {

  private final Integer id;

  private final LocalDate createdAt;

  private final LocalDate updatedAt;

  private String name;

  private String email;

  private String password;

  private String lastName;

  private String phoneNumber;

  private String profileImage;

  public UserEntity(Integer id, LocalDate createdAt, LocalDate updatedAt, String name,
                    String email, String password, String lastName, String phoneNumber,
                    String profileImage) {
    this.id = id;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.name = name;
    this.email = email;
    this.password = password;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.profileImage = profileImage;
  }

  public UserEntity(ResultSet rs) throws SQLException {
    this.id = rs.getInt("id");
    this.createdAt = rs.getDate("created_at").toLocalDate();
    this.updatedAt = rs.getDate("updated_at").toLocalDate();
    this.name = rs.getString("name");
    this.email = rs.getString("email");
    this.password = rs.getString("password");
    this.lastName = rs.getString("last_name");
    this.phoneNumber = rs.getString("phone_number");
    this.profileImage = rs.getString("profile_image");
  }

  public String getName() {
    return this.name;
  }

  @Override
  public String toString() {
    return """
            %s
            Id: %d
            Name: %s
            Email: %s
            Last Name: %s
            Phone Number: %s
            Created At: %s
            Updated At: %s
            %s
            """.formatted(CommonComponents.printTitle("User"),
                          this.id,
                          this.name,
                          this.email,
                          this.lastName,
                          this.phoneNumber,
                          this.createdAt,
                          this.updatedAt,
                          CommonComponents.printLine());
  }
}
