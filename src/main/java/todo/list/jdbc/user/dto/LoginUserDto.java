package todo.list.jdbc.user.dto;

import org.jetbrains.annotations.NotNull;

public record LoginUserDto(@NotNull String email, @NotNull String password) {

}
