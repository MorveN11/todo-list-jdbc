package todo.list.jdbc.interfaces;

import java.sql.Connection;
import java.util.List;

/**
 * This interface models the service.
 *
 * @param <T> The type of the service.
 */
public abstract class Service<T> {

  protected final Connection connection;

  public Service(Connection connection) {
    this.connection = connection;
  }

  /**
   * This method creates a new object.
   *
   * @param object The object to create.
   */
  public abstract void create(Object object);

  /**
   * This method gets an object.
   *
   * @param id The id of the object to get.
   * @return The object.
   */
  public abstract T get(int id);

  /**
   * This method updates an object.
   *
   * @param object The object to update.
   * @param id     The id of the object to update.
   */
  public abstract void update(Object object, int id);

  /**
   * This method deletes an object.
   *
   * @param id The id of the object to delete.
   */
  public abstract void delete(int id);

  /**
   * This method lists all the objects.
   *
   * @return The list of objects.
   */
  public abstract List<T> listAll();
}
