package todo.list.jdbc.priority;

import java.sql.Connection;
import todo.list.jdbc.priority.entities.PriorityEntity;

/**
 * This class is the controller of the priority.
 */
public class PriorityController {

  private final PriorityService priorityService;

  public PriorityController(Connection connection) {
    this.priorityService = new PriorityService(connection);
  }

  /**
   * This method creates a new priority.
   *
   * @param priority The priority to be created.
   */
  public void create(String priority) {
    this.priorityService.create(priority);
  }

  /**
   * This method gets a priority.
   *
   * @param id The id of the priority to get.
   * @return The priority.
   */
  public PriorityEntity get(int id) {
    return this.priorityService.get(id);
  }

  /**
   * This method updates a priority.
   *
   * @param priority The priority to update.
   * @param id       The id of the priority to update.
   */
  public void update(String priority, int id) {
    this.priorityService.update(priority,
                                id);
  }

  /**
   * This method deletes a priority.
   *
   * @param id The id of the priority to delete.
   */
  public void delete(int id) {
    this.priorityService.delete(id);
  }
}
