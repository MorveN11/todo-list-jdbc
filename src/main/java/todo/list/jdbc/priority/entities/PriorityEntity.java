package todo.list.jdbc.priority.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import todo.list.jdbc.priority.dto.UpdatePriorityDto;

/**
 * This class models the priority.
 */
public class PriorityEntity {

  private final Integer id;

  private final LocalDate createdAt;

  private final LocalDate updatedAt;

  private Integer todoId;

  private String name;

  /**
   * This constructor creates a new priority.
   *
   * @param id        The id of the priority.
   * @param todoId    The id of the todo.
   * @param name      The name of the priority.
   * @param createdAt The date the priority was created.
   * @param updatedAt The date the priority was updated.
   */
  public PriorityEntity(Integer id, Integer todoId, String name, LocalDate createdAt,
                        LocalDate updatedAt) {
    this.id = id;
    this.todoId = todoId;
    this.name = name;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  /**
   * This constructor creates a new priority.
   *
   * @param rs The result set of the priority.
   * @throws SQLException If an SQL error occurs.
   */
  public PriorityEntity(ResultSet rs) throws SQLException {
    this.id = rs.getInt("id");
    this.todoId = rs.getInt("todo_id");
    this.name = rs.getString("name");
    this.createdAt = rs.getDate("created_at").toLocalDate();
    this.updatedAt = rs.getDate("updated_at").toLocalDate();
  }

  public Integer getId() {
    return this.id;
  }

  public LocalDate getCreatedAt() {
    return this.createdAt;
  }

  public Integer getTodoId() {
    return this.todoId;
  }

  public void setTodoId(Integer todoId) {
    this.todoId = todoId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocalDate getUpdatedAt() {
    return this.updatedAt;
  }

  /**
   * This method updates the priority.
   *
   * @param updatePriorityDto The priority to update.
   */
  public void updatePriority(UpdatePriorityDto updatePriorityDto) {
    if (updatePriorityDto.todoId() != null) {
      this.todoId = updatePriorityDto.todoId();
    }
    if (updatePriorityDto.name() != null) {
      this.name = updatePriorityDto.name();
    }
  }
}
