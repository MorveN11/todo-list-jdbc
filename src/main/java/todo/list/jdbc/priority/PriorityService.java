package todo.list.jdbc.priority;

import java.sql.Connection;
import java.util.List;
import todo.list.jdbc.interfaces.Service;
import todo.list.jdbc.priority.entities.PriorityEntity;

/**
 * This class is the service of the priority.
 */
public class PriorityService extends Service<PriorityEntity> {

  public PriorityService(Connection connection) {
    super(connection);
  }

  @Override
  public void create(Object object) {
  }

  @Override
  public PriorityEntity get(int id) {
    return null;
  }

  @Override
  public void update(Object object, int id) {
  }

  @Override
  public void delete(int id) {
  }

  @Override
  public List<PriorityEntity> listAll() {
    return null;
  }
}
