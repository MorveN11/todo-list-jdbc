package todo.list.jdbc.priority.dto;

import org.jetbrains.annotations.NotNull;

/**
 * This class represents the data transfer object of the priority.
 *
 * @param todoId The id of the todo.
 * @param name   The name of the priority.
 */
public record CreatePriorityDto(@NotNull Integer todoId, @NotNull String name) {

}
