package todo.list.jdbc.priority.dto;

/**
 * DTO for updating a priority
 *
 * @param todoId The id of the todo.
 * @param name   The name of the priority.
 */
public record UpdatePriorityDto(Integer todoId, String name) {

}
