package todo.list.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import todo.list.jdbc.priority.PriorityController;
import todo.list.jdbc.todo.TodoController;
import todo.list.jdbc.user.UserController;

/**
 * This class manages the todo database.
 */
public class TodoDb {

  private final Connection connection;

  private final TodoController todoController;

  private final UserController userController;

  private final PriorityController priorityController;

  /**
   * This constructor creates a new todo database.
   *
   * @param connection The connection to the database.
   * @throws SQLException If the connection to the database fails.
   */
  public TodoDb(Connection connection) throws SQLException {
    this.connection = connection;
    if (this.connection == null) {
      throw new SQLException("Connection failed");
    }
    this.todoController = new TodoController(this.connection);
    this.userController = new UserController(this.connection);
    this.priorityController = new PriorityController(this.connection);
  }

  public TodoController getTodoController() {
    return this.todoController;
  }

  public UserController getUserController() {
    return this.userController;
  }

  public PriorityController getPriorityController() {
    return this.priorityController;
  }

  public void closeConnection() throws SQLException {
    this.connection.close();
  }
}
