package todo.list.jdbc.todo.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import todo.list.jdbc.todo.dto.UpdateTodoDto;

/**
 * This class represents a todo.
 */
public class TodoEntity {

  private final Integer id;

  private final LocalDate createdAt;

  private final LocalDate updatedAt;

  private String title;

  private String description;

  /**
   * This constructor creates a new todo.
   *
   * @param id          The id of the todo.
   * @param title       The title of the todo.
   * @param description The description of the todo.
   * @param createdAt   The creation date of the todo.
   * @param updatedAt   The update date of the todo.
   */
  public TodoEntity(Integer id, String title, String description, LocalDate createdAt,
                    LocalDate updatedAt) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  /**
   * This constructor creates a new todo.
   *
   * @param rs The result set.
   * @throws SQLException If the result set is invalid.
   */
  public TodoEntity(ResultSet rs) throws SQLException {
    this.id = rs.getInt("id");
    this.title = rs.getString("title");
    this.description = rs.getString("description");
    this.createdAt = rs.getDate("created_at").toLocalDate();
    this.updatedAt = rs.getDate("updated_at").toLocalDate();
  }

  public Integer getId() {
    return this.id;
  }

  public LocalDate getCreatedAt() {
    return this.createdAt;
  }

  public LocalDate getUpdatedAt() {
    return this.updatedAt;
  }

  /**
   * This method sets the todo.
   *
   * @param todo The todo to set.
   */
  public void setTodo(UpdateTodoDto todo) {
    if (todo.title() != null) {
      this.setTitle(todo.title());
    }
    if (todo.description() != null) {
      this.setDescription(todo.description());
    }
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return """
            Todo
            id: %d
            title: %s
            description: %s
            createdAt: %s
            updatedAt: %s
            """.formatted(this.id,
                          this.title,
                          this.description,
                          this.createdAt,
                          this.updatedAt);
  }
}
