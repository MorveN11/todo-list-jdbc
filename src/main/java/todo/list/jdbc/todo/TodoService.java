package todo.list.jdbc.todo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import todo.list.jdbc.interfaces.Service;
import todo.list.jdbc.todo.dto.CreateTodoDto;
import todo.list.jdbc.todo.dto.UpdateTodoDto;
import todo.list.jdbc.todo.entities.TodoEntity;

/**
 * This class models the todo.
 */
public class TodoService extends Service<TodoEntity> {

  public TodoService(Connection connection) {
    super(connection);
  }

  @Override
  public void create(Object object) {
    CreateTodoDto todo = (CreateTodoDto) object;
    String createToDoQuery = "INSERT INTO todo (title, description) VALUES (?, ?)";
    try (PreparedStatement preparedStatement = this.connection.prepareStatement(createToDoQuery)) {
      preparedStatement.setString(1,
                                  todo.title());
      preparedStatement.setString(2,
                                  todo.description());
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException("Error creating todo: " + e.getMessage(),
                                 e);
    }
  }

  @Override
  public TodoEntity get(int id) {
    String getToDoQuery = "SELECT * FROM todo WHERE id = ?";
    try (PreparedStatement preparedStatement = this.connection.prepareStatement(getToDoQuery)) {
      preparedStatement.setInt(1,
                               id);
      ResultSet rs = preparedStatement.executeQuery();
      if (rs.next()) {
        return new TodoEntity(rs);
      }
      return null;
    } catch (SQLException e) {
      throw new RuntimeException("Error getting todo: " + e.getMessage(),
                                 e);
    }
  }

  @Override
  public void update(Object object, int id) {
    UpdateTodoDto todo = (UpdateTodoDto) object;
    TodoEntity todoToUpdate = this.get(id);
    if (todoToUpdate == null) {
      throw new RuntimeException("Todo not found");
    }
    todoToUpdate.setTodo(todo);
    String updateToDoQuery = "UPDATE todo SET title = ?, description = ? WHERE id = ?";
    try (PreparedStatement preparedStatement = this.connection.prepareStatement(updateToDoQuery)) {
      preparedStatement.setString(1,
                                  todoToUpdate.getTitle());
      preparedStatement.setString(2,
                                  todoToUpdate.getDescription());
      preparedStatement.setInt(3,
                               todoToUpdate.getId());
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException("Error updating todo: " + e.getMessage(),
                                 e);
    }
  }

  @Override
  public void delete(int id) {
    String deleteToDoQuery = "DELETE FROM todo WHERE id = ?";
    try (PreparedStatement preparedStatement = this.connection.prepareStatement(deleteToDoQuery)) {
      preparedStatement.setInt(1,
                               id);
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException("Error deleting todo: " + e.getMessage(),
                                 e);
    }
  }

  @Override
  public List<TodoEntity> listAll() {
    String listAllQuery = "SELECT * FROM todo";
    try (PreparedStatement preparedStatement = this.connection.prepareStatement(listAllQuery)) {
      ResultSet rs = preparedStatement.executeQuery();
      List<TodoEntity> todos = new ArrayList<>();
      while (rs.next()) {
        todos.add(new TodoEntity(rs));
      }
      return todos;
    } catch (SQLException e) {
      throw new RuntimeException("Error listing todos: " + e.getMessage(),
                                 e);
    }
  }
}
