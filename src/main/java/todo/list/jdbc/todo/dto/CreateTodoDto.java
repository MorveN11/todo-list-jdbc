package todo.list.jdbc.todo.dto;

import org.jetbrains.annotations.NotNull;

/**
 * This class models the todo.
 *
 * @param title       The title of the todo.
 * @param description The description of the todo.
 */
public record CreateTodoDto(@NotNull String title, @NotNull String description) {

}
