package todo.list.jdbc.todo.dto;

/**
 * This class models the todo.
 *
 * @param title       The title of the todo.
 * @param description The description of the todo.
 */
public record UpdateTodoDto(String title, String description) {

}
