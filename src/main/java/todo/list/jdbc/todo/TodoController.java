package todo.list.jdbc.todo;

import java.sql.Connection;
import java.util.List;
import todo.list.jdbc.todo.dto.CreateTodoDto;
import todo.list.jdbc.todo.dto.UpdateTodoDto;
import todo.list.jdbc.todo.entities.TodoEntity;

/**
 * This class models the todo.
 */
public class TodoController {

  protected final Connection connection;

  private final TodoService todoService;

  public TodoController(Connection connection) {
    this.connection = connection;
    this.todoService = new TodoService(connection);
  }

  /**
   * This method creates a new todo.
   *
   * @param todo The todo to create.
   */
  public void create(CreateTodoDto todo) {
    this.todoService.create(todo);
  }

  /**
   * This method gets a todo.
   *
   * @param id The id of the todo to get.
   * @return The todo.
   */
  public TodoEntity get(int id) {
    return this.todoService.get(id);
  }

  /**
   * This method updates a todo.
   *
   * @param todo The todo to update.
   * @param id   The id of the todo to update.
   */
  public void update(UpdateTodoDto todo, int id) {
    this.todoService.update(todo,
                            id);
  }

  /**
   * This method deletes a todo.
   *
   * @param id The id of the todo to delete.
   */
  public void delete(int id) {
    this.todoService.delete(id);
  }

  /**
   * This method lists all the todos.
   */
  public List<TodoEntity> listAll() {
    return this.todoService.listAll();
  }

  /**
   * This method updates the title of a todo.
   *
   * @param title The title to update.
   * @param id    The id of the todo to update.
   */
  public void updateTitle(String title, int id) {
    this.todoService.update(new UpdateTodoDto(title,
                                              null),
                            id);
  }
}
