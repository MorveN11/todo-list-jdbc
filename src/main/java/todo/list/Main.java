package todo.list;

import static todo.list.utils.Constants.PASSWORD;
import static todo.list.utils.Constants.URL;
import static todo.list.utils.Constants.USERNAME;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import todo.list.jdbc.TodoDb;
import todo.list.ui.App;

/**
 * This class is the entry point of the application.
 */
public class Main {

  /**
   * This method is the entry point of the application.
   *
   * @param args The command line arguments.
   * @throws SQLException If the connection to the database fails.
   */
  public static void main(String[] args) throws SQLException, IOException {
    Connection connection = DriverManager.getConnection(URL,
                                                        USERNAME,
                                                        PASSWORD);
    TodoDb db = new TodoDb(connection);
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
    App app = new App(db,
                      br,
                      bw);
    app.initApp();
  }
}
