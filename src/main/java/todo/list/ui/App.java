package todo.list.ui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.SQLException;
import todo.list.jdbc.TodoDb;
import todo.list.ui.components.login.LoginMenu;

/**
 * This class models the app.
 */
public class App {

  private final LoginMenu loginMenu;

  /**
   * This constructor creates a new app.
   *
   * @param db The todo database.
   */
  public App(TodoDb db, BufferedReader br, BufferedWriter bw) {
    this.loginMenu = new LoginMenu(db,
                                   br,
                                   bw);
  }

  /**
   * This method initializes the app.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void initApp() throws IOException, SQLException {
    loginMenu.initComponent();
  }
}
