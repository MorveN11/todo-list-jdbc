package todo.list.ui.interfaces;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.SQLException;
import todo.list.jdbc.TodoDb;

/**
 * This interface is used to get the reader and writer from the terminal.
 */
public abstract class Component {

  private final BufferedReader br;

  private final BufferedWriter bw;

  private final TodoDb db;

  /**
   * This constructor creates a new terminal component.
   *
   * @param br The buffered reader.
   * @param bw The buffered writer.
   */
  public Component(TodoDb db, BufferedReader br, BufferedWriter bw) {
    this.br = br;
    this.bw = bw;
    this.db = db;
  }

  public abstract void initComponent() throws IOException, SQLException;

  /**
   * This method is used to get the buffered reader.
   *
   * @return The buffered reader.
   */
  public BufferedReader getReader() {
    return br;
  }

  /**
   * This method is used to get the buffered writer.
   *
   * @return The buffered writer.
   */
  public BufferedWriter getWriter() {
    return bw;
  }

  /**
   * This method is used to get the database.
   *
   * @return The database.
   */
  public TodoDb getDb() {
    return db;
  }

  public void printComponent() throws IOException {
    bw.write(this.toString());
    bw.flush();
  }

  /**
   * This method is used to validate the menu option.
   *
   * @throws IOException If an I/O error occurs.
   */
  public void exitApplication() throws IOException, SQLException {
    db.closeConnection();
    bw.close();
    br.close();
    System.exit(0);
  }
}
