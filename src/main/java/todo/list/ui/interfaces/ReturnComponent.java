package todo.list.ui.interfaces;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import todo.list.jdbc.TodoDb;

/**
 * This interface is used to get the reader and writer from the terminal.
 *
 * @param <T> The type of the return component.
 */
public abstract class ReturnComponent<T> extends TerminalComponent {

  /**
   * This constructor creates a new return component.
   *
   * @param db The todo database.
   * @param br The buffered reader.
   * @param bw The buffered writer.
   */
  public ReturnComponent(TodoDb db, BufferedReader br, BufferedWriter bw) {
    super(db,
          br,
          bw);
  }

  @Override
  public void initComponent() {
  }

  public abstract T initReturnComponent() throws IOException;
}
