package todo.list.ui.interfaces;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import todo.list.jdbc.TodoDb;
import todo.list.utils.CommonComponents;
import todo.list.utils.ValidateInputs;

/**
 * This interface is used to get the reader and writer from the terminal.
 */
public abstract class TerminalComponent extends Component {

  /**
   * This constructor creates a new terminal component.
   *
   * @param db The todo database.
   * @param br The buffered reader.
   * @param bw The buffered writer.
   */
  public TerminalComponent(TodoDb db, BufferedReader br, BufferedWriter bw) {
    super(db,
          br,
          bw);
  }

  /**
   * This method is used to print the component.
   *
   * @return The component.
   * @throws IOException If an I/O error occurs.
   */
  public int validateMenuOption() throws IOException {
    int option = -1;
    while (option < 1) {
      String stOption = getReader().readLine();
      stOption = stOption.trim();
      if (ValidateInputs.validateInt(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = Integer.parseInt(stOption);
    }
    return option;
  }

  private void printComponentInvalidInput() throws IOException {
    getWriter().write(CommonComponents.invalidInput() + "\n");
    this.printComponent();
  }

  /**
   * This method is used to read the string.
   *
   * @param message The message.
   * @return The string.
   * @throws IOException If an I/O error occurs.
   */
  public Integer readInt(String message) throws IOException {
    getWriter().write(message);
    getWriter().flush();
    return validateInt();
  }

  /**
   * This method is used to validate the int.
   *
   * @return The int.
   * @throws IOException If an I/O error occurs.
   */
  private Integer validateInt() throws IOException {
    int option = -1;
    while (option < 1) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (ValidateInputs.validateInt(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = Integer.parseInt(stOption);
    }
    return option;
  }

  private String getInput() throws IOException {
    getWriter().write(CommonComponents.cancelInput() + "\n");
    getWriter().flush();
    String stOption = getReader().readLine();
    stOption = stOption.trim();
    if (stOption.equalsIgnoreCase("cancel")) {
      getWriter().write(CommonComponents.canceledInput() + "\n");
      getWriter().flush();
      return null;
    }
    return stOption;
  }

  /**
   * This method is used to read the string.
   *
   * @param message The message.
   * @return The string.
   * @throws IOException If an I/O error occurs.
   */
  public String readString(String message) throws IOException {
    getWriter().write(message + "\n");
    getWriter().flush();
    return validateString();
  }

  /**
   * This method is used to validate the string.
   *
   * @return The string.
   * @throws IOException If an I/O error occurs.
   */
  private String validateString() throws IOException {
    String option = "";
    while (option.isEmpty()) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateString(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = stOption;
    }
    return option;
  }

  /**
   * This method is used to validate the uuid.
   *
   * @return The uuid.
   * @throws IOException If an I/O error occurs.
   */
  public Boolean validateBoolean() throws IOException {
    boolean option = false;
    boolean output = false;
    while (!option) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateBoolean(stOption.toLowerCase())) {
        printComponentInvalidInput();
        continue;
      }
      output = Boolean.parseBoolean(stOption.toLowerCase());
      option = true;
    }
    return output;
  }

  /**
   * This method is used to read the string.
   *
   * @param message The message.
   * @return The string.
   * @throws IOException If an I/O error occurs.
   */
  public String readEmail(String message) throws IOException {
    getWriter().write(message + "\n");
    getWriter().flush();
    return validateEmail(message);
  }

  /**
   * This method is used to validate the uuid.
   *
   * @return The uuid.
   * @throws IOException If an I/O error occurs.
   */
  public String validateEmail(String message) throws IOException {
    String option = "";
    while (option.isEmpty()) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateEmail(stOption)) {
        printProcedureInvalidInput(message);
        continue;
      }
      option = stOption;
    }
    return option;
  }

  private void printProcedureInvalidInput(String message) throws IOException {
    write(CommonComponents.invalidInput() + "\n" + message);
  }

  public void write(String input) throws IOException {
    getWriter().write(input + "\n");
    getWriter().flush();
  }

  /**
   * This method is used to validate the uuid.
   *
   * @return The uuid.
   * @throws IOException If an I/O error occurs.
   */
  public Integer validateYear() throws IOException {
    int option = -1;
    while (option < 1) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateYear(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = Integer.parseInt(stOption);
    }
    return option;
  }
}
