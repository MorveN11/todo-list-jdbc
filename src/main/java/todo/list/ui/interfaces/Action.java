package todo.list.ui.interfaces;

/**
 * This class is the base class for all actions.
 */
public abstract class Action {

  private final TerminalComponent component;

  public Action(TerminalComponent component) {
    this.component = component;
  }

  protected TerminalComponent getComponent() {
    return component;
  }
}
