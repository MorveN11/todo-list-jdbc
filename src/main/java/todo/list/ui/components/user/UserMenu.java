package todo.list.ui.components.user;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.SQLException;
import todo.list.jdbc.TodoDb;
import todo.list.jdbc.user.entities.UserEntity;
import todo.list.ui.interfaces.TerminalComponent;
import todo.list.utils.CommonComponents;

/**
 * This class models the user menu.
 */
public class UserMenu extends TerminalComponent {

  private final UserEntity user;

  /**
   * This constructor creates a new terminal component.
   *
   * @param db The todo database.
   * @param br The buffered reader.
   * @param bw The buffered writer.
   */
  public UserMenu(TodoDb db, BufferedReader br, BufferedWriter bw, UserEntity user) {
    super(db,
          br,
          bw);
    this.user = user;
  }

  @Override
  public void initComponent() throws IOException, SQLException {
    boolean flag = false;
    while (!flag) {
      printComponent();
      int option = validateMenuOption();
      switch (option) {
        case 1 -> write("Create Todo");
        case 2 -> write("List Todos");
        case 3 -> flag = true;
        case 4 -> {
          exitApplication();
          return;
        }
        default -> write(CommonComponents.invalidInput());
      }
    }
  }

  @Override
  public String toString() {
    return """
            %s
            1. Create todo
            2. List todos
            3. Back
            4. Exit Application
            %s
            """.formatted(CommonComponents.printTitle("User Menu - %s".formatted(this.user.getName())),
                          CommonComponents.printLine());
  }
}
