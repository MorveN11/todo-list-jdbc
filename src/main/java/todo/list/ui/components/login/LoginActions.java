package todo.list.ui.components.login;

import java.io.IOException;
import todo.list.jdbc.user.entities.UserEntity;
import todo.list.ui.interfaces.Action;

public class LoginActions extends Action {

  public LoginActions(LoginMenu menu) {
    super(menu);
  }

  public UserEntity login() throws IOException {
    getComponent().write("Login as user");
    String email = getComponent().readEmail("Enter your email: ");
    if (email == null) {
      return null;
    }
    String password = getComponent().readString("Enter your password: ");
    if (password == null) {
      return null;
    }
    UserEntity user = getComponent().getDb().getUserController().login(email,
                                                                       password);
    if (user == null) {
      return null;
    }
    getComponent().write("Login successfully!\n");
    return user;
  }
}
