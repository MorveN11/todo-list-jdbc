package todo.list.ui.components.login;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.SQLException;
import todo.list.jdbc.TodoDb;
import todo.list.jdbc.user.entities.UserEntity;
import todo.list.ui.components.user.UserMenu;
import todo.list.ui.interfaces.TerminalComponent;
import todo.list.utils.CommonComponents;

/**
 * This class models the login menu.
 */
public class LoginMenu extends TerminalComponent {

  private final LoginActions loginAction;

  /**
   * This constructor creates a new login menu.
   *
   * @param db The todo database.
   * @param br The buffered reader.
   * @param bw The buffered writer.
   */
  public LoginMenu(TodoDb db, BufferedReader br, BufferedWriter bw) {
    super(db,
          br,
          bw);
    this.loginAction = new LoginActions(this);
  }

  @Override
  public void initComponent() throws IOException, SQLException {
    while (true) {
      printComponent();
      int option = validateMenuOption();
      switch (option) {
        case 1 -> {
          UserEntity user = loginAction.login();
          if (user != null) {
            new UserMenu(getDb(),
                         getReader(),
                         getWriter(),
                         user).initComponent();
          } else {
            write("Login failed");
          }
        }
        case 2 -> {
          exitApplication();
          return;
        }
        default -> write(CommonComponents.invalidInput());
      }
    }
  }

  @Override
  public String toString() {
    return """
            %s
            1. Login As User
            2. Exit Application
            %s
            """.formatted(CommonComponents.printTitle("Login Menu"),
                          CommonComponents.printLine());
  }
}
