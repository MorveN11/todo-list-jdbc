package todo.list.utils;

/**
 * This class contains methods that are used to print messages to the user.
 */
public class CommonComponents {

  /**
   * This method is used to print the main menu.
   *
   * @param title The title of the menu.
   * @return The menu.
   */
  public static String printTitle(String title) {
    return String.format("%s %s %s",
                         Constants.HALF_LINE,
                         title,
                         Constants.HALF_LINE);
  }

  public static String printLine() {
    return Constants.LINE;
  }

  public static String invalidInput() {
    return Constants.INVALID_INPUT;
  }

  public static String cancelInput() {
    return Constants.CANCEL_INPUT;
  }

  public static String canceledInput() {
    return Constants.CANCELED_INPUT;
  }
}
