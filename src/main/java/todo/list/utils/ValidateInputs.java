package todo.list.utils;

import static todo.list.utils.Constants.EMAIL_REGEX;
import static todo.list.utils.Constants.STRING_REGEX;
import static todo.list.utils.Constants.YEAR_REGEX;

import java.util.regex.Pattern;

/**
 * This class contains the methods to validate the inputs.
 */
public class ValidateInputs {

  public static boolean validateInt(String input) {
    return !input.matches("-?\\d+");
  }

  public static boolean validateString(String input) {
    return input.matches(STRING_REGEX) && validateNotEmpty(input);
  }

  public static boolean validateNotEmpty(String input) {
    return !input.isEmpty();
  }

  public static boolean validateEmail(String email) {
    return email.matches(EMAIL_REGEX) && validateNotEmpty(email);
  }

  public static boolean validateBoolean(String input) {
    return input.equalsIgnoreCase("true") || input.equalsIgnoreCase("false");
  }

  public static boolean validateYear(String year) {
    return Pattern.matches(YEAR_REGEX,
                           year);
  }
}
