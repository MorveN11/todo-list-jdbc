package todo.list.utils;

/**
 * This class contains the constants used in the application.
 */
public class Constants {

  public static final String URL = "jdbc:mysql://localhost:7706/todo_list_db";

  public static final String USERNAME = "root";

  public static final String PASSWORD = "root";

  public static final String EMAIL_REGEX = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";

  public static final String STRING_REGEX = "[a-zA-Z0-9 !@#$%^&*(),.?\":{}|<>\\\\\\\\-_+=;/'`~]+";

  protected static final String LINE = "--------------------------";

  protected static final String HALF_LINE = LINE.substring(0,
                                                           LINE.length() / 2);

  protected static final String INVALID_INPUT = "Invalid input. Please try again.";

  protected static final String CANCEL_INPUT = "Write 'cancel' to cancel.";

  protected static final String CANCELED_INPUT = "Operation canceled.";

  protected static final String YEAR_REGEX = "^(19[0-9]{2}|20[0-9]{2})$";
}
