plugins {
    id("java")
    id("application")
}

group = "todo.list"
version = "1.0"

repositories {
    mavenCentral()
}

application {
    mainClass.set("todo.list.Main")
}

dependencies {
    implementation("org.jetbrains:annotations:24.0.1")
    implementation("mysql:mysql-connector-java:8.0.33")
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.9.2")
}

tasks.test {
    useJUnitPlatform()
}

tasks.register("dockerDown") {
    doFirst {
        exec {
            commandLine("docker", "compose", "down")
        }
    }
}

tasks.register("dockerRemove") {
    doFirst {
        exec {
            commandLine("docker", "volume", "rm", "--force", "todo-list_data")
        }
    }
}

tasks.register("dockerUp") {
    doFirst {
        exec {
            commandLine("docker", "compose", "up", "-d")
        }
    }
    doLast {
        println("\n-- Connecting to the Database! --\n")
        Thread.sleep(12 * 1000)
    }
}

tasks.run<JavaExec> {
    dependsOn("dockerDown")
    dependsOn("dockerRemove")
    dependsOn("dockerUp")
    standardInput = System.`in`
}

tasks.jar {
    manifest {
        attributes["Main-Class"] = application.mainClass.get()
    }
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    archiveFileName.set("${project.name}-${project.version}.jar")
}
