# Todo List

by Manuel Morales

## Dependencies

To run this project you need to have installed:

- Java 17
- Gradle 7.2
- Docker 20.10.8

## Run project

To run this project please execute this commands:

```bash
./gradlew build
./gradlew run
```
